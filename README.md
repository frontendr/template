# Template engine

A simple template engine loosely based on the logic of CakePHP and Django templates.

## Installing

Installation is easiest by using [Composer](http://www.getcomposer.org/).

* 1: Install Composer: `$ curl -sS https://getcomposer.org/installer | php -- --install-dir=bin`
* 2: Add the following to your `composer.json`:
```
#!json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:frontendr/template.git"
        }
    ],
    "require": {
        "frontendr/template": "master"
    }
}

```
* 3: Then run `$ php bin/composer.phar update` to update the dependencies. **Note:** You might need to add `"minimum-stability": "dev"` to your `composer.json` to be able to install.
* 4: Add the autoloader to your project:
```
#!php
<?php
require 'vendor/autoload.php';
// The class Frontendr\Template is now available
```

## Usage

### Create an instance

Minimum configuration example
```
#!php
<?php
$tpl = Frontendr\Template([
    // (required) tell where the templates are located:
    'paths' => 'path/to/templates',
]);

$tpl->set('someVariable', 'some value');

echo $tpl->render('my-template');
```

### Templates
The default extension for a template is `.tpl.php`. This can be changed by using `$tpl->setTemplateExtension('.xxx');`.
Your template file `/path/to/templates/my-template.tpl.php` could look like this:
```
#!php
<?php
// You can use plain PHP/HTML
?>
<p>Hello, this is my variable: <?php echo $someVariable; ?></p>
```

#### The `TemplateContext`
When rendering a template a template has access to an instance of `TemplateContext` with `$this`. This class holds methods which allows extending templates with blocks and gives access to `TemplateHelper` classes. 

```
#!php
<?php
// To extend another template simply call extend()
$this->extend('parent-template');

// Now we can capture content in blocks which can be used in one of the parent templates
$this->start('my-block');
?>
<p>This content is added to 'my-block'</p>
<?php
$this->end();
?>
```
Then in the parent template `parent-template.tpl.php`:
```
#!php
<p>Here is the block:</p>
<?php echo $this->fetch('my-block', 'Default value in case it is not given by another template'); ?>
```
You can also `assign` `append` or `prepend` to a block or use `super()` to get the content of extending blocks.
```
#!php
<?php
$this->block('test');
echo 'A';
$this->end();

// The block 'test' is now 'A'

$this->append('test');
echo 'B';
$this->end();

// The block 'test' is now 'AB'

$this->prepend('test');
echo 'C';
$this->end();

// The block 'test' is now 'CAB'
```

## API Documentation
Generate API documentation: `$ php vendor/bin/phpdoc.php`

## Testing
Run unit tests: `$ phpunit --bootstrap tests/bootstrap.php tests`