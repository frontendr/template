<?php
/**
 * Template class
 *
 * @author Johan Arensman <johan@frontendr.com>
 */

namespace Frontendr;

/**
 * Class available in the template context to provide helper functions.
 *
 * @package Template
 * @version 1.0
 */
class TemplateHelper {

    /**
     * @var array $settings
     */
    public $__settings = array();

    /**
     * @param array $settings
     */
    public function __construct(array $settings) {
        $this->__settings = array_replace_recursive($this->__settings, $settings);
    }

}
