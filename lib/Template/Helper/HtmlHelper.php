<?php
/**
 * Template class
 *
 * @author Johan Arensman <johan@frontendr.com>
 */

namespace Frontendr;

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'TemplateHelper.php';

/**
 * HTML Helper class
 *
 * @package Template
 */
class HtmlHelper extends TemplateHelper {

    /**
     * @var string Base URL used for URL functions
     */
    private $baseURL;

    /**
     * @var string Base URL used for static URL functions
     */
    private $staticBaseURL;

    /**
     * @var string Path to static files
     */
    private $staticPath;

    /**
     * @var array Default settings for the HTML helper
     */
    public $__settings = [
        'baseUrl' => null,
        'staticBaseUrl' => null,
        'staticPath' => null,
        'paths' => [
            'img' => 'img/',
            'css' => 'css/',
            'js' => 'js/'
        ]
    ];

    /**
     * @param array $settings
     */
    public function __construct(array $settings) {
        parent::__construct($settings);

        // Set the base path:
        $parts = explode('/', $_SERVER['SCRIPT_FILENAME']);
        $indexFile = array_pop($parts);

        $this->baseURL = $this->__settings['baseUrl'];
        if (is_null($this->baseURL)) {
            $this->baseURL = substr($_SERVER['SCRIPT_NAME'], 0, -strlen($indexFile));
        }

        $this->staticBaseURL = $this->__settings['staticBaseUrl'];
        if (is_null($this->staticBaseURL)) {
            $this->staticBaseURL = $this->baseURL;
        }

        $this->staticPath = $this->__settings['staticPath'];
        if (is_null($this->__settings['staticPath'])) {
            $this->staticPath = ''; 
        }
    }

    /**
     * Returns the absolute url to the given path
     *
     * @param string $path The path to create the URL for
     *
     * @return string
     */
    public function url($path) {
        $base = $this->baseURL;
        return $base . ltrim($path, '/');
    }

    /**
     * Returns the absolute path to the given image in the image folder
     *
     * @param string $path
     *
     * @return string
     */
    public function img($path) {
        return $this->staticUrl(__FUNCTION__, $path);
    }

    /**
     * Returns the absolute path to the given css file in the css folder
     *
     * @param string $path
     *
     * @return string
     */
    public function css($path) {
        return $this->staticUrl(__FUNCTION__, $path);
    }

    /**
     * Returns the absolute path to the given javascript file in the javascript folder
     *
     * @param string $path
     *
     * @return string
     */
    public function js($path) {
        return $this->staticUrl(__FUNCTION__, $path);
    }

    /**
     * Returns the absolute path to the given asset type
     *
     * @param string $type Type of asset
     * @param string $path
     *
     * @return string
     */
    private function staticUrl($type, $path) {
        $base = $this->staticBaseURL;
        $parts = empty($this->staticPath) ? [] : [$this->staticPath];
        if (array_key_exists($type, $this->__settings['paths'])) {
            array_push($parts, $this->__settings['paths'][$type]);
        }
        array_push($parts, $path);
        return $this->pathJoin($base, $parts);
    }

    /**
     * @param string $path
     * @param string|array $partN...
     *
     * @return string The parts joined by DIRECTORY_SEPARATOR
     */
    private function pathJoin($path, $partN) {
        $glue = DIRECTORY_SEPARATOR;

        if (is_array($partN)) {
            $parts = $partN;
        } else {
            $parts = func_get_args();
            array_shift($parts);
        }

        foreach ($parts as $part) {
            if (substr($path, -1) !== $glue) {
                $path .= $glue;
            }
            $path .= ltrim($part, $glue);
        }
        return $path;
    }
}
