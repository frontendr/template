<?php
/**
 * @var Frontendr\TemplateContext $this
 * @var array                    $article
 */
$this->extend('layouts/default');

$this->block('content');
?>
    <main class="main-content">
        <?php echo $this->fetch('main-content'); ?>
    </main>

    <div class="side-bar">
        <?php echo $this->fetch('side-bar'); ?>
    </div>
<?php
$this->end();
