<!doctype html>
<html>
<head>
    <title><?php echo $this->fetch('page-title', 'Example'); ?></title>
    <link rel="stylesheet" href="<?php echo $this->Html->css('example.css'); ?>">
    <?php
    // put your css and javascript in blocks so extending templates can add more when needed
    echo $this->fetch('head-css');
    echo $this->fetch('head-javascript');
    ?>
</head>

<body>

<div class="page-wrapper">

    <header class="page-header">
        <?php $this->block('page-header'); ?>
        <h1>Example website!</h1>
        <?php $this->end(); ?>
    </header>

    <?php
    // other templates define the lay-out of the content area of the page:
    echo $this->fetch('content');
    ?>

    <footer class="page-footer">
        <p>Copyright <?php echo date('Y'); ?>, <a href="http://www.frontendr.com/">Frontendr</a></p>
    </footer>

</div>

<?php echo $this->fetch('body-javascript'); ?>
</body>

</html>