<?php
/**
 * @var Frontendr\TemplateContext $this
 * @var array                    $article
 * @var array                    $archive
 */
$this->extend('layouts/content-with-sidebar');

$this->block('main-content');
?>
<article>
    <header>
        <h2><?php echo $article['title']; ?></h2>
    </header>

    <?php echo $article['text']; ?>
</article>
<?php
$this->end();

$this->block('side-bar');
?>
<aside>
    <h2>Other articles</h2>
    <ul>
        <?php
        // I use the 'alternative syntax' which I think is more suitable for templates.
        // http://php.net/manual/en/control-structures.alternative-syntax.php
        foreach ($archive as $article): ?>
            <li>
                <a href="#"><?php echo $article['title']; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</aside>
<?php
$this->end();
?>
