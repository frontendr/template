<?php
/**
 * This is an example project to 
 */
// Some basic settings so we can se errors and no caching is getting in the way:
error_reporting(E_ALL);
ini_set('display_errors', 1);
if (function_exists('opcache_reset')) {
    opcache_reset();
}

// Path settings, feel free to change. The constants are not required.
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)) . DS);

require_once ROOT . 'lib/Template/Template.php';

// Create a new template instance:
$tpl = new Frontendr\Template([
        // the path (or paths) setting is the only required setting:
        'path' => [ROOT . 'examples/templates'],
        // we'll add the Html helper for easy static linking:
        'helpers' => [
            'Html'
        ]
    ]
);

// This part should be in some sort of controller or page handler.
// We are adding data to our template:
$article = [
    'title' => 'Creating a website with the Template engine',
    'text' => "<p>It's easier than you think!</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem beatae debitis dicta eligendi enim est
hic, ipsa maxime minima, molestiae nemo nesciunt qui quibusdam quo quod vitae voluptatem voluptatibus.</p>"
];

// Add the variables to the template using the set() method.
// We add an associative array but a single variable is also possible with $tpl->set('myvar', 'myvalue'); 
$tpl->set([
    'article' => $article,
    'archive' => [
        [
            'title' => 'Why template engines rock!',
        ],
        [
            'title' => 'Templating inside out vs outside in'
        ],
        [
            'title' => 'Creating helpers for frequent tasks'
        ]
    ]
]);

// Finally render the 'inner' template
echo $tpl->render('article');
