<?php
/**
 * Requires the class Template
 */
date_default_timezone_set('Europe/Amsterdam');

define('DS', DIRECTORY_SEPARATOR);
require_once dirname(dirname(__FILE__)) . DS . 'lib' . DS . 'Template' . DS . 'Template.php';
