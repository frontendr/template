<?php
use Frontendr\Template;

/**
 * Class TemplateTest
 * 
 * Tests for loading and basic rendering of templates
 */
class TemplateTest extends PHPUnit_Framework_TestCase {

    /**
     * Returns the templates directory for these tests
     *
     * @return string
     */
    private function getTemplatesDir($postfix = '') {
        return dirname(__FILE__) . DS . 'templates' . DS . $postfix;
    }

    /**
     * @dataProvider templateInstanceProvider
     *
     * @param Template $tpl a Template instance
     */
    public function testTemplateExists($tpl) {
        $this->assertTrue($tpl->templateExists('basic/template-1'));
    }

    /**
     * @dataProvider templateInstanceProvider
     *
     * @param Template $tpl a Template instance
     */
    public function testMultipleTemplateDirs($tpl) {
        $tpl->addTemplatePath($this->getTemplatesDir('basic'));
        $tpl->addTemplatePath($this->getTemplatesDir('basic2'));

        // test if we can find 'template-1'
        $this->assertTrue($tpl->templateExists('template-1'));

        // now test if we get the correct template (the last added template path):
        $this->assertEquals($this->getTemplatesDir('basic2/template-1.tpl.php'),
            $tpl->getTemplatePath('basic2/template-1')
        );
    }

    /**
     * @dataProvider templateInstanceProvider
     * 
     * @param Template $tpl a Template instance
     */
    public function testTemplateExtension($tpl) {
        $tpl->setTemplateExtension('.html');

        $this->assertTrue($tpl->templateExists('basic/htmlExtension'));
    }

    /**
     * Provides a Template instance
     *
     * @return array
     */
    public function templateInstanceProvider() {
        return [
            [
                $tpl = new Template(['path' => [$this->getTemplatesDir()]])
            ]
        ];
    }
}
