<?php
use Frontendr\Template;

/**
 * Class TemplateTest
 * 
 * Tests for the use of variables in templates.
 */
class VariablesTest extends PHPUnit_Framework_TestCase {

    /**
     * Returns the templates directory for these tests
     *
     * @return string
     */
    private function getTemplatesDir($postfix = '') {
        return dirname(__FILE__) . DS . 'templates' . DS . $postfix;
    }

    /**
     * @dataProvider templateInstanceProvider
     *
     * @param Template $tpl a Template instance
     */
    public function testSingleVariable($tpl) {
        $value = 'ok!';
        $tpl->set('singleVariable', $value);
        $result = $tpl->render('variables/single-variable');
        $this->assertEquals($value, $result);
    }

    /**
     * @dataProvider templateInstanceProvider
     *
     * @param Template $tpl a Template instance
     */
    public function testMultipleVariables($tpl) {
        $variables = [
            'var1' => 1,
            'var2' => 2,
            'var3' => 3,
        ];
        $tpl->set($variables);
        $result = $tpl->render('variables/multiple-variables');
        $this->assertEquals(implode(',', array_values($variables)), $result);
    }

    /**
     * @dataProvider templateInstanceProvider
     *
     * @param Template $tpl a Template instance
     */
    public function testVariableInExtendingTemplates($tpl) {
        $value = 'extending!';
        $tpl->set('singleVariable', $value);
        $result = $tpl->render('variables/variable-in-extending-template');
        $this->assertEquals($value, $result);
    }



    /**
     * Provides a Template instance
     *
     * @return array
     */
    public function templateInstanceProvider() {
        return [
            [
                $tpl = new Template(['path' => [$this->getTemplatesDir()]])
            ]
        ];
    }
}
