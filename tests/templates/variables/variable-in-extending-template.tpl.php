<?php
/**
 * @var Frontendr\TemplateContext $this
 * @var string $singleVariable
 */
$this->extend('variables/single-variable');
$this->block('variable');
echo $singleVariable;
$this->end();