<?php
/**
 * @var Frontendr\TemplateContext $this
 * @var string $singleVariable
 */
$this->block('variable');
echo $singleVariable;
$this->end();