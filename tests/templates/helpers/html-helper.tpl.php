<?php
/**
 * @var Frontendr\TemplateContext $this
 * @var string $image
 */
echo $this->Html->img($image);