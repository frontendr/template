<?php
use Frontendr\Template;

/**
 * Class TemplateTest
 * 
 * Tests for rendering of templates
 */
class RenderingTest extends PHPUnit_Framework_TestCase {

    /**
     * Returns the templates directory for these tests
     *
     * @return string
     */
    private function getTemplatesDir($postfix = '') {
        return dirname(__FILE__) . DS . 'templates' . DS . $postfix;
    }

    /**
     * @dataProvider templateInstanceProvider
     *
     * @param Template $tpl a Template instance
     */
    public function testRenderSimpleTemplate($tpl) {
        $result = $tpl->render('basic/template-1');
        $this->assertStringEqualsFile($this->getTemplatesDir('basic/template-1.tpl.php'), $result);
    }

    /**
     * @dataProvider templateInstanceProvider
     *
     * @param Template $tpl a Template instance
     */
    public function testExtendingTemplate($tpl) {
        $this->assertEquals('ABC', $tpl->render('extending/a'));
    }

    /**
     * Test the HTML helper
     */
    public function testHtmlHelper() {
        $tpl = new Template([
            'path' => [$this->getTemplatesDir()],
            'helpers' => [
                'Html' => [
                    'paths' => [
                        'img' => 'test_images/'
                    ]
                ]
            ]
        ]);
        $tpl->set('image', 'foo.png');
        $this->assertStringEndsWith('test_images/foo.png', $tpl->render('helpers/html-helper'));
    }

    /**
     * Provides a Template instance
     *
     * @return array
     */
    public function templateInstanceProvider() {
        return [
            [
                $tpl = new Template(['path' => [$this->getTemplatesDir()]])
            ]
        ];
    }
}
